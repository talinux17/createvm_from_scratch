#!/bin/bash
# Add user student, config logging, add rateExam script.
bashistConfFile="/etc/profile.d/bashist.sh"
bashist4rsyslog="/etc/rsyslog.d/bashist.conf"
histCmdsFileName=hist_cmds
histCmdsPath="/var/log"
histCmdsLog="$histCmdsPath/$histCmdsFileName"

echo "Final stage, adding user(s), logging, asses_exam_script, etc."
useradd -m student
echo student:student | chpasswd

#Log commands history
if ! $(grep local4.info $bashist4rsyslog >/dev/null 2>&1); then
   cat > $bashist4rsyslog << EOF
# Log history of bash commands
local4.info $histCmdsLog
EOF
fi
service rsyslog restart
if ! $(grep PROMPT_COMMAND $bashistConfFile >/dev/null 2>&1); then
   cat > $bashistConfFile <<- EOF
	# Log any bash cmd - see rsyslog.conf 4 destination
	shopt -s histappend
	HISTSIZE=999999
	readonly PROMPT_COMMAND='retVal=$?; history -a >(tee -a ~/.bash_history | logger -p local4.info -t "$USER[$PWD] $SSH_CONNECTION [$retVal]")'
	EOF
fi

