#!/bin/bash

tmpFile="changelog.tmp"
logFile="changelog.txt"

touch $logFile
cp $logFile $tmpFile
echo "* $(date "+%a %b %d %Y, version %Y%m%dxx, Cosmin M. <talinux17@gmail.com>")" > $logFile
echo "- " >> $logFile
echo >> $logFile
cat $tmpFile >> $logFile
rm $tmpFile
vim +2 -c 'startinsert!' $logFile
