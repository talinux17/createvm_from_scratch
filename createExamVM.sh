#!/bin/bash

### Script to create VM in VirtualBox starting from an .iso file - kind of unattending install
### Firstly will create "VM structure" as per VirtualBox needs, an xml file that will hold the machine virtual hardware config.
### Secondly will prepare unattended install params (user, pw &co) and will fire up the so prepared install process
### Then it will start the provision phase, starting with updating OS (with reboot if need it).

# Define vars
vmName="centos6_examTiny"
vmOS="RedHat_64"
vmDescr="Centos 6 minimal instalation for exam Linux1 & 2 /Telacad"
ksCfgFile="createExamVM_ks.cfg"
isoPath="$HOME/isos"
isoImage="CentOS-6.10-x86_64-minimal.iso"
diskName="$vmName.vdi"
diskPath=""
diskSize=7168				# in MB
machineFolder=
sshPortFW=22022
sshHost='localhost'
sshKey='autoinst.id_rsa'
vmUser='root'
vmPw='telacad'
sTime=$(date +%s)
eTime=

# Define functions
getMachineFolder() {
   OS=$(uname -s)
   case "$OS" in
      Linux)
         machineFolder="$(dirname "$(echo $(VBoxManage showvminfo centos6_examTiny | grep -oP 'Config file:\K.*'))")"
	 ### echo here seems redundant, but removes leading blank spaces from grep's output so dirname's happy
	 ;;
      Darwin)
	 machineFolder="$(VboxManage list systemproperties | grep 'Default machine folder:' | grep -o '/.*')/$vmName"
	 ;;
   esac
   echo $machineFolder
}
isVMrunning() {
# returns true if VM is runnin
   VBoxManage list runningvms | grep -qow $vmName
   return $?
}
isOSuntagged() {
###!!! this can be refined to check for the presence of a random tag /label sent as param
   VBoxManage getextradata $vmName OS_installed | grep -qow "No value set"
   return $?
}
sshPokeWait() {
   sshPokeCmd="ssh -qi $sshKey -p $sshPortFW -l $vmUser -o StrictHostKeyChecking=no -o ConnectTimeout=1 $sshHost echo >/dev/null"
   #echo $sshPokeCmd
   echo; echo -n "Started ssh poking each second! "
   while ! $(echo "$sshPokeCmd"); do
      echo -n "."
      sleep 1
   done
}
shTimeLapse() {
   echo "Time so far: ~$(($(date +%s) -$sTime))s"
}

###   BUILDING PHASE   >>>
# Test if isos folder exists, if not make it.
[ -d "$isoPath" ] || (echo -n "The folder $isoPath/ doesn't exist, creating it... "; mkdir "$isoPath"; echo created!;)
# Start working on VM...
# Check if VM structure (xml file) exists in VirtualBox, if not create it
[ $(VBoxManage list vms | grep -w $vmName >/dev/null; echo $?) -eq 0 ] || VBoxManage createvm --name $vmName --ostype $vmOS --register
# Now set all of its params
if ! $(isVMrunning); then
# If VM is not running
   # General stuff
   VBoxManage modifyvm $vmName --clipboard bidirectional --draganddrop bidirectional --description "$vmDescr"

   # Motherboard
   VBoxManage modifyvm $vmName --memory 1024 --mouse usbtablet --apic on --chipset ich9
   if $(isOSuntagged); then
      VBoxManage modifyvm $vmName --boot1 dvd --boot2 disk --boot3 none --boot4 none
   fi
   #CPU
   VBoxManage modifyvm $vmName --cpus 2 --cpuhotplug on

   # GPU /Dispaly
   VBoxManage modifyvm $vmName --vram 128 --accelerate3d on

   # Network
   ###!!! Need to refine this. I get VBoxManage: error: A NAT rule of this name already exists
   VBoxManage modifyvm $vmName --nictype1 virtio
   VBoxManage modifyvm $vmName --natpf1 ssh,tcp,,$sshPortFW,,22
   VBoxManage modifyvm $vmName --nic4 nat
   VBoxManage modifyvm $vmName --nictype4 virtio

   # Storage, adds it if not exists
   if ! $(VBoxManage showvminfo $vmName | grep -o "Sata Controller" > /dev/null); then
      diskPath="$(getMachineFolder)/$diskName"
      VBoxManage createmedium disk --filename "$diskPath" --size $diskSize --format VDI
      VBoxManage storagectl $vmName --name "Sata Controller" --add sata --controller intelAhci --portcount 4
      VBoxManage storageattach $vmName --storagectl "Sata Controller" --port 0 --device 0 --type hdd --medium "$diskPath"
   fi
   if ! $(VBoxManage showvminfo $vmName | grep -o "IDE Controller" > /dev/null); then
      VBoxManage storagectl $vmName --name "IDE Controller" --add ide --controller ICH6 --hostiocache on
      VBoxManage storageattach $vmName --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium $isoPath/$isoImage
   fi
fi
shTimeLapse
###   BUILDING PHASE ENDS   <<<

###   OS INSTALL PHASE (kickstart)   >>>
# Light it up...
if $(vboxmanage getextradata $vmName OS_installed | grep -q "No value set"); then
# Skip it if OS is alreaty installed /tagged on the VM
   if [ -s $isoPath/$isoImage -a -s $ksCfgFile ]; then
	   VBoxManage unattended install "$vmName" --iso="$isoPath/$isoImage" --password="$vmPw" --user=$vmUser --locale=en_US --country=RO --hostname=c6.exam --script-template="$ksCfgFile"  --post-install-template='exit.sh' --start-vm=headless
      if [ "$?" -ne 0 ]; then echo "Unattended Install setup failed preparatory phase!"; exit; fi
   else
      echo "File(s) not found. Check if $isoPath/$isoImage or $ksCfgFile exist and are in the right path."
      exit
   fi
   # Wait for installer to finish
   echo; echo -n "Waiting for installer to poweroff the machine after finish "
   while ! $(vboxmanage showvminfo $vmName --machinereadable| grep VMState=.poweroff >/dev/null); do
      echo -n "."
      sleep 1
   done
   # Tag the VM with a tag=OS_installed so that we know that we've done this phase
   vboxmanage setextradata $vmName "OS_installed" "$(date +%F-%H:%M)"
   VBoxManage modifyvm $vmName --boot1 disk --boot2 none --boot3 none --boot4 none
   echo " Done!"
   shTimeLapse
fi
echo Booting it up for provision /configuration stage after 1s.
sleep 1		# Plenty of sleep is needed if --start-vm=gui above (hw dependent)
if ! $(isVMrunning); then
   echo "Starting up VM $vmName..."
   VBoxManage startvm $vmName --type headless
   if [ "$?" -ne 0 ]; then echo "Can't start VM, exiting..."; fi
   echo; echo Waiting 12 seconds before poking ssh into it...
   sleep 12
fi
if [ ! -s $sshKey ]; then echo "ssh private key not found! Exiting..."; exit; fi
# Right-ing ssh key perms
#if ! $(stat -c %a $sshKey | grep -qwE "[64]00"); then
if ! $(ls -l $sshKey | cut -d" " -f1 | grep -qwE "[r][w-][-]{7}"); then
   chmod 600 $sshKey
   echo "Changed file perms for private ssh key $sshKey to 600..."
fi
sshPokeWait
echo ' Done!'
echo '### Provision ready to start. ###'
###   OS INSTALL (kickstart) ENDS    <<<

###   PROVISION PHASE   >>>
#"$(< provisionVM.sh)"
. provisionVM.sh
###   PROVISION PHASE ENDS   <<<

#while ! $(ssh -qi $sshKey -p $sshPortFW -l $vmUser -o StrictHostKeyChecking=no -o ConnectTimeout=1 $sshHost echo >/dev/null); do

