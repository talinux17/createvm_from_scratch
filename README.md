# createVM_from_scratch

Create & configure a Virtual(Box) Machine starting from .iso file via bash scripts.

Shall work in Linux & MacOS.

Tested with:
- VirtualBox &ge; 6.0.0 
- centos 6.10 - now EOL and moved to https://vault.centos.org/6.10/
