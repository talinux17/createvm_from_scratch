#!/bin/bash
shopt -s expand_aliases
alias sshCmd="ssh -qi $sshKey -p $sshPortFW -l $vmUser -o StrictHostKeyChecking=no -o ConnectTimeout=7 $sshHost 'bash -s'"
isReboot=
isInternet=
vboxGA="additions"
vboxDownloadURI="https://download.virtualbox.org/virtualbox"
vboxGAprefix="VBoxGuestAdditions_"
vboxGAiso=
vboxVer=
uwServFile="unwanted.txt"

# Stage 0: fix the fuckin' locale & ssh login speed ('UseDNS no')
if [ -s "$uwServFile" ]; then
   scp -qi $sshKey -P $sshPortFW -o StrictHostKeyChecking=no "$uwServFile" $vmUser@$sshHost:
fi
echo
sshCmd < provision.00_initial_fix

# Don't go any further if we can't go internet from inside VM ($? != 7)
isInternet=$?
if [ $isInternet -ne 7 ]; then
   echo "No internet access from the Virtual Machine!!! Exiting! "
   shTimeLapse
   exit 2
fi
shTimeLapse

# Stage 1: Update OS; if kernel update is detected, then reboot.
echo
sshCmd < provision.01_update
isReboot=$?
# if isReboot=42, then we had a reboot
shTimeLapse

# Stage 2: Install neeeded packs.
if [ $isReboot -eq 42 ]; then
   echo "Waiting 12s before going next stage: package installation..."
   sleep 12
fi
sshPokeWait
echo " Done!"
echo
sshCmd < provision.02_install
shTimeLapse

# Stage 3: Install vbox guest additions
echo
vboxVersion=$(vboxmanage -v)
vboxVer=$(echo $vboxVersion | cut -dr -f1)
case $vboxVersion in
# known VirtualBox bad (guest additions) versions
   6.0.0r127566)
      vboxVer=5.2.22
      vboxGAiso="$vboxGAprefix$vboxVer.iso"
   ;;
esac
if [ -n "$vboxGAiso" ]; then
# if len of $vboxGAiso is not zero, it means we have a bad GA from actual Virtualbox and need a proper iso form a previous version
   vboxGA="$isoPath/$vboxGAiso"
   if [ ! -s $vboxGA ]; then
      echo "Can't find Guest Additions iso file at $isoPath/ so trying to get it from $vboxDownloadURI"
      vboxGAiso=$(curl -s $vboxDownloadURI/$vboxVer/ | grep iso | cut -f2 -d'"')
      if [ ! $vboxGAiso ]; then
         echo "GA iso file not found at $vboxDownloadURI/$vboxVer/ Exiting!"
	 exit
      fi
      wget -P "$isoPath/" "$vboxDownloadURI/$vboxVer/$vboxGAiso"
      if [ $? -ne 0 ]; then echo "Error while downloading $vboxGAiso exiting!"; exit; fi
   fi
fi 

vboxmanage storageattach $vmName --storagectl "IDE Controller" --port 0 --device 0 --medium "$vboxGA" --forceunmount
if [ $? -ne 0 ]; then echo "There was an issue attaching Guest Additions CD-Rom."; exit; fi
sshCmd < provision.03_install_ga $vboxVer
shTimeLapse

# Stage 4: Final configuration.
echo
sshCmd < provision.04_final_touch.sh
shTimeLapse

#ssh -qi $sshKey -p $sshPortFW -l $vmUser -o StrictHostKeyChecking=no -o ConnectTimeout=7 $sshHost 'bash -s' < provision.initial_fix
